/*
 *File: DockerSandbox.js
 *Author: Osman Ali Mian/Asad Memon
 *Created: 3rd June 2014
 *Revised on: 25th June 2014 (Added folder mount permission and changed executing user to nobody using -u argument)
 *Revised on: 30th June 2014 (Changed the way errors are logged on console, added language name into error messages)
 */


/**
 * @Constructor
 * @variable DockerSandbox
 * @description This constructor stores all the arguments needed to prepare and execute a Docker Sandbox
 * @param {Number} timeout_value: The Time_out limit for code execution in Docker
 * @param {String} path: The current working directory where the current API folder is kept
 * @param {String} folder: The name of the folder that would be mounted/shared with Docker container, this will be concatenated with path
 * @param {String} vm_name: The TAG of the Docker VM that we wish to execute
 * @param {String} compiler_name: The compiler/interpretor to use for carrying out the translation
 * @param {String} file_name: The file_name to which source code will be written
 * @param {String} code: The actual code
 * @param {String} output_command: Used in case of compilers only, to execute the object code, send " " in case of interpretors
 */
var DockerSandbox = function(timeout_value,path,folder,parameters,vm_name,params_conteneur,compiler_name,file_name,code,output_command,languageName,e_arguments,tests,user)
{

    this.timeout_value=timeout_value;
    this.path=path;
    this.folder=folder;
    this.parameters=parameters;
    this.params_conteneur = params_conteneur;
    this.vm_name=vm_name;
    this.compiler_name=compiler_name;
    this.file_name=file_name;
    this.code = code;
    this.output_command=output_command;
    this.langName=languageName;
    if(output_command=="nul"){
	this.extra_arguments=parameters;
    }
    else{
	this.extra_arguments=e_arguments;
		if(this.extra_arguments == ""){
			this.extra_arguments = '" "';
		}
    }
    this.tests=tests;
    this.user=user;
}


/**
 * @function
 * @name DockerSandbox.run
 * @description Function that first prepares the Docker environment and then executes the Docker sandbox 
 * @param {Function pointer} success ?????
 */
DockerSandbox.prototype.run = function(success)
{
    var sandbox = this;

    this.prepare( function(){
	sandbox.execute(success);
    });
}


/*
 * @function
 * @name DockerSandbox.prepare
 * @description Function that creates a directory with the folder name already provided through constructor
 * and then copies contents of folder named Payload to the created folder, this newly created folder will be mounted
 * on the Docker Container. A file with the name specified in file_name variable of this class is created and all the
 * code written in 'code' variable of this class is copied into this file.
 * Summary: This function produces a folder that contains the source file and 2 scripts, this folder is mounted to our
 * Docker container when we run it.
 * @param {Function pointer} success ?????
 */
DockerSandbox.prototype.prepare = function(success)
{
    var exec = require('child_process').exec;
    var fs = require('fs');
    var sandbox = this;

    exec("mkdir -p "+ this.path+this.folder + "/inputFile.d " + this.path+this.folder + "/paramsFile.d " + this.path+this.folder + "/outputFile.d && cp "+this.path+"/Payload/* "+this.path+this.folder+"&& chmod 777 -R "+ this.path+this.folder,function(st)
		{
			fs.writeFile(sandbox.path + sandbox.folder+"/" + sandbox.file_name, sandbox.code,function(err)
				{
					if (err)
					{
						console.log(err);
					}
					else
					{
						console.log(sandbox.langName+" file was saved!");
						exec("chmod 777 \'"+sandbox.path+sandbox.folder+"/"+sandbox.file_name+"\'")

						var i=0;
						for(test in sandbox.tests){
							fs.writeFile(sandbox.path + sandbox.folder+"/inputFile.d/"+i, "stdin" in sandbox.tests[test] ? sandbox.tests[test].stdin : "", function(err)
								{
									if (err)
									{
										console.log(err);
									}
									else
									{
										console.log("Input file was saved!");
									}
							});
							fs.writeFile(sandbox.path + sandbox.folder+"/paramsFile.d/"+i, "params" in sandbox.tests[test] ? sandbox.tests[test].params : "", function(err)
								{
									if (err)
									{
										console.log(err);
									}
									else
									{
										console.log("Params file was saved!");
									}
							});
							i+=1;
						}

						success();

					}
			});
    });
}

/*
 * @function
 * @name DockerSandbox.execute
 * @precondition: DockerSandbox.prepare() has successfully completed
 * @description: This function takes the newly created folder prepared by DockerSandbox.prepare() and spawns a Docker container
 * with the folder mounted inside the container with the name '/usercode/' and calls the script.sh file present in that folder
 * to carry out the compilation. The Sandbox is spawned ASYNCHRONOUSLY and is supervised for a timeout limit specified in timeout_limit
 * variable in this class. This function keeps checking for the file "Completed" until the file is created by script.sh or the timeout occurs
 * In case of timeout an error message is returned back, otherwise the contents of the file (which could be the program output or log of 
 * compilation error) is returned. In the end the function deletes the temporary folder and exits
 * 
 * Summary: Run the Docker container and execute script.sh inside it. Return the output generated and delete the mounted folder
 *
 * @param {Function pointer} success ?????
 */

DockerSandbox.prototype.executer_conteneur=function(success){
    var exec = require('child_process').exec;
    var execSync = require('child_process').execSync;
    var fs = require('fs');

    //Réinitialise le conteneur
    if(this.code=="reset" && execSync('docker ps -a -q --no-trunc|grep -c ' + this.parameters + ' || true') >0){
	try{
	    execSync('docker stop ' + this.parameters);
            this.parameters=null;
	}
	catch(err){
	    resultat="Erreur";
	    console.log("Erreur : " + err['stderr'].toString());
	}
    }

    //Cherche le conteneur auquel on veut se connecter
    if(this.parameters!=null && this.parameters!="" && this.parameters!=0 && execSync('docker ps -a -q --no-trunc|grep -c ' + this.parameters + ' || true') >0){ //|| true pour que la commande n'échoue jamais
	console.log("Resume : " + this.parameters);
	var resultat='invalide';
	try{
	    if(this.code!=null && this.code!=''){
		//Exécute le test
		//code_exec=this.code.replace(/([\\\/"$`()])/g, "\\$1");
		code_exec="docker exec -t " + this.parameters + " bash -c \"if " + this.code.replace(/([\\\"$])/g, "\\$1") + "; then echo valide; else echo invalide; fi\"";
		console.log("Test : " + code_exec);
		resultat=execSync(code_exec, {shell: '/bin/bash'}).toString();
		console.log("Résultat : " + resultat);
	    }
	    var net_info=JSON.parse(execSync('docker inspect '+this.parameters));
	    if(net_info[0]['NetworkSettings']['Ports'].hasOwnProperty('4200/tcp'))
                //Port particulier ouvert
		success(this.parameters, net_info[0]['NetworkSettings']['IPAddress'], net_info[0]['NetworkSettings']['Ports']['4200/tcp'][0]['HostPort'], resultat);
	    else
		success(this.parameters, net_info[0]['NetworkSettings']['IPAddress'], 0, resultat);
	}
	catch(err){
	    resultat="Erreur";
	    console.log("Erreur : " + err['stderr'].toString());
	    success(0, "", "", err['stderr'].toString());
	}
    }
    else{
	if(this.vm_name=="redirect")
	    var st = 'bash -c "docker run --rm --cap-add=NET_ADMIN -h ' + this.vm_name + ' -d '+ this.vm_name + '"';
	else
	    var st = 'bash -c "docker run --rm -p 4200 ' + this.params_conteneur+' -h '+this.vm_name+' -d '+this.vm_name + '"';

	//log the statement in console
	console.log(st);

	//execute the Docker
	try{
	    var contid=execSync(st).toString().trim();
	    console.log("------------------------------")
	    console.log("Created : "+contid);

	    var net_info=JSON.parse(execSync('docker inspect '+contid));

	    //Démarre le chrono du timeout
	    exec("docker stop --time=" + this.timeout_value + " " + contid);
	}
	catch(err){
	    resultat="Erreur";
	    console.log("Erreur : " + err);
	    success(0, "", "", err);
	}

	if(net_info[0]['NetworkSettings']['Ports'].hasOwnProperty('4200/tcp'))
            //Port particulier ouvert
	    success(contid, net_info[0]['NetworkSettings']['IPAddress'], net_info[0]['NetworkSettings']['Ports']['4200/tcp'][0]['HostPort'], '');
	else
	    success(contid, net_info[0]['NetworkSettings']['IPAddress'], 0, '');


	//now remove the temporary directory
	console.log("ATTEMPTING TO REMOVE: " + this.folder);
	console.log("------------------------------")
	exec("rm -r " + this.folder);
    }

    return contid;
}

DockerSandbox.prototype.executer_compilation=function(success){
    var exec = require('child_process').exec;
    var execSync = require('child_process').execSync;
    var fs = require('fs');

    var sandbox=this;

    var myC = 0; //variable to enforce the timeout_value
    //this statement is what is executed
    var st = this.path+'DockerTimeout.sh ' + this.timeout_value + 's --rm -u user -e \'NODE_PATH=/usr/local/lib/node_modules\' -e LANG=fr_CA.utf8 -i -t -v  "' + this.path + this.folder + '":/usercode ' + this.vm_name + ' /usercode/script.sh ' + this.compiler_name + ' ' + this.file_name + ' ' + this.output_command+ ' ' + this.extra_arguments;

    if(this.langName=="MySQL"){
		var net_info=JSON.parse(execSync('docker inspect '+ this.parameters));
		var ip=net_info[0]['NetworkSettings']['IPAddress'];

		st+=" -h "+ip+" -u " + this.user;
    }

    //log the statement in console
    console.log(st);

    //execute the Docker, This is done ASYNCHRONOUSLY
    exec(st);
    console.log("------------------------------")
    //Check For File named "completed" after every 1 second

    const intid = setInterval( () =>
		{
			myC = myC + 1;
			
			if (myC < sandbox.timeout_value*10 && ! fs.existsSync (sandbox.path + sandbox.folder + '/completed')) {
				return;
			}

			if (myC >= sandbox.timeout_value*10 ){
				console.log("TIMEOUT");
			}

			clearInterval(intid);

			var résultats=[];

			for(var test in sandbox.tests){
				//Stdout
				try{
					data = fs.readFileSync(sandbox.path + sandbox.folder + '/outputFile.d/' + test, 'utf8');
					var lines = data.toString().split('*-COMPILEBOX::ENDOFOUTPUT-*')
					data=lines[0]
					var time=lines[1]
					résultats[test] = {
						output: data,
						errors: "",
						time: time
					}
				}
				catch(err){
					résultats[test] = {
						output: "",
						errors: "TIMEOUT",
						time: 0
					}
				}

				// Stderr
				try{
					data_err = fs.readFileSync(sandbox.path + sandbox.folder + '/outputFile.d/' + test + '.errors', 'utf8');
					résultats[test].errors = data_err
				}
				catch(err){
					//Pas de fichier d'erreur
				}
				
			}

			success(résultats);

			//now remove the temporary directory
			console.log("ATTEMPTING TO REMOVE: " + sandbox.folder);
			console.log("------------------------------")
//			exec("rm -r " + sandbox.folder);

		}, 100);
}


DockerSandbox.prototype.execute = function(success)
{
    var execSync = require('child_process').execSync;
    var sandbox = this;

    console.log( new Date() );
    try{
		if(this.compiler_name=='sshd'){
			this.executer_conteneur(success);
		}
		else if(this.langName=='MySQL'){
			var contid=this.parameters;
			var contip;
			var contport;
			var res_valide;
			var data;
			var exec_time;
			var errors;
			//Redémarre le conteneur au besoin

			if(this.code=="reset" || execSync('docker ps -a -q --no-trunc|grep -c ' + this.parameters + ' || true') ==0){ //|| true pour que la commande n'échoue jamais
				//this.vm_name=this.parameters;
				this.executer_conteneur(function(id, ip, port, valide){
					contid=id;
					contip=ip;
					contport=port;
					res_valide=valide;
				});
				this.parameters=contid;
				this.extra_arguments=contid;
			}
			this.vm_name="remotecompiler";
			this.executer_compilation(function(sortie, temps, erreurs){
				data=sortie;
				exec_time=temps;
				errors=erreurs;
				success(contid, contip, contport, res_valide, data, exec_time, errors);
			});
		}
		//Sinon, une compilation ordinaire
		else{
			this.executer_compilation(success);
		}
    }catch(err){
		console.log(err);
    }
}

module.exports = DockerSandbox;
