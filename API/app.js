#!/usr/bin/node
/*
 *File: app.js
 *Author: Asad Memon / Osman Ali Mian
 *Last Modified: 5th June 2014
 *Revised on: 30th June 2014 (Introduced Express-Brute for Bruteforce pnotection)
 */


var express = require('express');
var arr = require('./compilers');
var sandBox = require('./DockerSandbox');
var app = express.createServer();
const port=process.env.CB_PORT ?? 12380;
const timeout_comp = process.env.CB_TIMEOUT_COMP ?? 3;
const timeout_cont = process.env.CB_TIMEOUT_CONT ?? 600;

//var ExpressBrute = require('express-brute');
//var store = new ExpressBrute.MemoryStore(); // stores state locally, don't use this in production
//var bruteforce = new ExpressBrute(store,{
//    freeRetries: 2,
//    minWait: 500
//});
//
app.use(express.static(__dirname));
app.use(express.bodyParser());

app.all('*', function(req, res, next) 
		{
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
			res.header('Access-Control-Allow-Headers', 'Content-Type');

			next();
		});

function random(size) {
    //returns a crypto-safe random
    return require("crypto").randomBytes(size).toString('hex');
}


app.post('/compile',function(req, res) 
		 {
			 
			 var language = req.body.language;
			 var code = req.body.code;

			 var tests=[];
			 var parameters=req.body.parameters;
			 
			 if ("tests" in req.body){
				 tests = req.body.tests;
			 }
			 
			 if ("stdin" in req.body){
				 var stdin = "stdin" in req.body ? req.body.stdin : "";
				 var params = "parameters" in req.body ? req.body.parameters : "";

				 tests.push( {stdin: stdin, params: params} );
			 }
			 
			 var params_conteneur = req.body.params_conteneur;

			 var folder= 'temp/' + random(10); //folder in which the temporary folder will be saved
			 var path=__dirname+"/"; //current working path

			 var vm_name=req.body.vm_name; //name of virtual machine that we want to execute

			 if(arr.compilerArray[language][0]=="sshd" || arr.compilerArray[language][3]=="MySQL" && vm_name!="remotecompiler"){
				 var timeout_value=timeout_cont;//Timeout Value, In Seconds
			 }
			 else{
				 var timeout_value=timeout_comp;//Timeout Value, In Seconds
			 }

			 var user = req.body.user;

			 //details of this are present in DockerSandbox.js
			 var sandboxType = new sandBox(timeout_value,path,folder,parameters,vm_name,params_conteneur, arr.compilerArray[language][0],arr.compilerArray[language][1],code,arr.compilerArray[language][2],arr.compilerArray[language][3],arr.compilerArray[language][4],tests,user);
			 
			 //data will contain the output of the compiled/interpreted code
			 //the result maybe normal program output, list of error messages or a Timeout error
			 if(arr.compilerArray[language][0]=="sshd"){
				 sandboxType.run(function(contid, ip, port, res_valid)
								 {
									 res.send({cont_id:contid, add_ip:ip, add_port:port, resultat:res_valid});
								 });
			 }
			 else if(arr.compilerArray[language][3]=="MySQL"){ 
				 sandboxType.run(function(contid, ip, port, res_valid, data, exec_time, err)
								 {
									 //Terrible hack pour empêcher le crash. À RÉGLER!!!
									 try{
										 res.send({cont_id:contid, add_ip:ip, add_port:port, resultat:res_valid, output:data, langid: language,code:code, errors:err, time:exec_time});
									 }
									 catch(err){
										 console.log(err);
									 }
								 });
			 }
			 else{
				 sandboxType.run(function(résultat)
								 {
									 //Terrible hack pour empêcher le crash. À RÉGLER!!!
									 try{
										 //console.log("Data: received: "+ data)
										 res.send(résultat);
									 }
									 catch(err){
										 console.log(err);
									 }
								 });
			 }

		 });

console.log("Listening at "+port)

//N'accepte que les connexions locales
app.listen(port, '0.0.0.0');
