# Compilebox

## 1. Dépendances obligatoires
- [docker](https://www.docker.com/)
- [nodejs](https://nodejs.org/en/)
- [npm](https://github.com/nodesource/distributions)

## 2. Installation & Configuration
### 2.1 Les permissions docker
- Ajouter l\'utilisateur courant au groupe docker. Sur Ubuntu :
```
sudo adduser $USER docker
```
- Exécuter la commande (la fermeture et réouverture de le session est nécessaire pour que le changement prenne effet globalement).
```
exec su -l $USER
```

### 2.2 Compilation de l\'image docker
- Allez vers le répertoire **/Setup** et construisez l'image docker
```
cd Setup
docker build -t remotecompiler .
```
- **Le point à la fin est important**

### 2.3 Installation des pré-requis nodejs
- Allez vers le répertoire **/API** et installez **express**
```
cd ../API
npm install express
```

### 2.4 Les permissions utilisateurs
- Assurez-vous que l\'utilisateur courant possède les droits d\'écriture dans le répertoire **/API/temp**
```
chmod u=rwx temp
```

### 2.5 Démarrage de l\'application
- Entrez la commande suivante et le serveur devrait démarrer avec le message**"Listening on 12380"**
```
node app.js
```

## 3. FAQ
Q: Comment ajouter l\'utilisateur courant au groupe docker sur Manjaro ?
- Utiliser la commande suivante :
```
sudo gpasswd -a $USER docker
```

Q: Media change: please insert the disc labeled
 'Ubuntu 20.04.1 LTS _Focal Fossa_ - Release amd64 (20200731)' in the drive '/media/cdrom/' and press [Enter]
- Utiliser les commandes suivantes :
```
sudo sed -i '/cdrom/d' /etc/apt/sources.list
grep -v '#' /etc/apt/sources.list
npm compilebox
```
